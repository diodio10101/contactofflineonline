package com.example.contact2.data.model

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

fun ViewGroup.inflate(@LayoutRes r:Int):View{
    return LayoutInflater.from(context).inflate(r,this,false)
}


fun List<Int>.sokka():Int{
    return size
}



fun Fragment.openFragmentAddBackStack(fm: Fragment, @IdRes resId:Int,name: String? = null) {
    parentFragmentManager.beginTransaction()
        .replace(resId, fm)
        .addToBackStack(name)
        .commit()
}

fun Fragment.popUpBackStack() {
    parentFragmentManager.popBackStack()
}

fun Fragment.popUpBackStack(name: String?, inclusive: Int = 0) {
    parentFragmentManager.popBackStack(name, inclusive)
}
fun FragmentActivity.addFragment(@IdRes containerId: Int, fm: Fragment) {
    supportFragmentManager
        .beginTransaction()
        .add(containerId, fm)
        .commit()
}