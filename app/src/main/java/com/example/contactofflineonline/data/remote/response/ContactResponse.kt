package com.example.contact3.data.remote.response

import com.example.contactofflineonline.data.model.enums.StatusEnum

data class ContactResponse(
    val id: Int,
    val firstName: String,
    val lastName : String,
    val phone: String
)
fun ContactResponse.toUIData():ContactUIData=ContactUIData(id,firstName,lastName,phone,StatusEnum.DEF)