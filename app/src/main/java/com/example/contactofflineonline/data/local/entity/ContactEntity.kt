package com.example.contactofflineonline.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.contact3.data.remote.response.ContactUIData
import com.example.contactofflineonline.data.model.enums.StatusEnum
import com.example.contactofflineonline.data.model.enums.toStatusEnum

@Entity
data class ContactEntity(
    @PrimaryKey(autoGenerate = true)val id:Int,
    val firstName:String,
    val lastName:String,
    val phone:String,
    val remoteID:Int=-1,
    val status:Int=1
)
fun ContactEntity.toUIData(id:Int):ContactUIData=
    ContactUIData(id,this.firstName,this.lastName,this.phone,status.toStatusEnum())