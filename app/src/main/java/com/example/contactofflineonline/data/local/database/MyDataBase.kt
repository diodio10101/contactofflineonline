package com.example.contactofflineonline.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.contactofflineonline.data.local.dao.ContactDao
import com.example.contactofflineonline.data.local.entity.ContactEntity

@Database(entities = [ContactEntity::class], version = 1)
abstract class MyDataBase:RoomDatabase() {
    abstract fun contactDao(): ContactDao;
}