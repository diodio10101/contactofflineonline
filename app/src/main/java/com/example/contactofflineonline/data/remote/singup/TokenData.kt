package com.example.contectapi.data.remote.singup

import com.example.contactofflineonline.data.sourse.MyShar


data class TokenData(
    val token:String,
    val phone:String
){
    init{
        MyShar.setToken(token)
    }
}