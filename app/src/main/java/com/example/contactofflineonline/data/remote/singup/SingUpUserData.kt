package com.example.contectapi.data.remote.singup

data class SingUpUserData(
    val firstName:String,
    val lastName:String,
    val phone:String,
    val password:String,
)