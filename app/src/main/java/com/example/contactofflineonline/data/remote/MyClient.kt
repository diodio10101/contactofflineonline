package com.example.contact3.data.remote

import android.content.Context
import com.example.contactofflineonline.utils.NetworkStatusValidator
//import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

//class MyClient {
//    lateinit var retrofit:Retrofit
//    @Inject private lateinit var networkStatusValidator:NetworkStatusValidator
//    fun init(context: Context) {
//        val cacheSize = (50 * 1024 * 1024).toLong()  // 50 MB
//        val cache = Cache(context.cacheDir, cacheSize)
//        val maxStale = 60 * 60 * 24 * 30
//
//        val okHttpClient =
//            OkHttpClient.Builder()
////                .addInterceptor(ChuckInterceptor(context))
//                .addInterceptor { chain ->
//                    if (!networkStatusValidator.hasInternet) {
//                        val newRequest = chain.request()
//                            .newBuilder()
//                            .header(
//                                "Cache-Control",
//                                "public, only-if-cached, max-stale=" + maxStale
//                            )
//                            .removeHeader("Pragma")
//                            .build()
//
//                        chain.proceed(newRequest)
//                    } else chain.proceed(chain.request())
//                }
//                .readTimeout(30, TimeUnit.SECONDS)
//                .connectTimeout(30, TimeUnit.SECONDS)
//                .writeTimeout(30, TimeUnit.SECONDS)
//                .cache(cache)
//                .build()
//        retrofit= Retrofit.Builder()
//            .baseUrl("https://b49c-195-158-16-140.ngrok-free.app/")
//            .addConverterFactory(GsonConverterFactory.create())
//            .client(okHttpClient)
//            .build()
//    }
//
//
//}