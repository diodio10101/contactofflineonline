package com.example.contact3.data.remote.request

data class CreateContactRequest(
    val firstName : String,
    val lastName : String,
    val phone : String
)
