package com.example.contectapi.data.remote.singup

data class SMSCodeData(
    val phone:String,
    val code:String
)