package com.example.contact3.data.remote.response

import com.example.contactofflineonline.data.model.enums.StatusEnum

data class ContactUIData(
    val id: Int,
    val firstName: String,
    val lastName : String,
    val phone: String,
    var status:StatusEnum
)