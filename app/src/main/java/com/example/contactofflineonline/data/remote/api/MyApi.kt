package com.example.contact3.data.remote.api

import com.example.contact3.data.remote.request.CreateContactRequest
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contectapi.data.remote.singup.IdData
import com.example.contectapi.data.remote.singup.LoginData
import com.example.contectapi.data.remote.singup.SMSCodeData
import com.example.contectapi.data.remote.singup.SingUpUserData
import com.example.contectapi.data.remote.singup.StringData
import com.example.contectapi.data.remote.singup.TokenData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query

interface MyApi {
    @POST("api/v1/register")
    fun singUpUser(@Body data:SingUpUserData):Call<StringData>

    @POST("api/v1/login")
    fun loginUser(@Body data:LoginData):Call<TokenData>

    @POST("api/v1/register/verify")
    fun smsCode(@Body data: SMSCodeData):Call<TokenData>

    @POST("api/v1/contact")
    fun addContact(@Body data:CreateContactRequest,@Header("token") token: String):Call<ContactResponse>

    @PUT("api/v1/contact")
    fun editContact(@Body data:ContactResponse, @Header("token") token:String):Call<ContactResponse>

    @DELETE("api/v1/contact")
    fun deleteContact(@Query("id") id:Int,@Header("token") token: String):Call<IdData>

    @GET("api/v1/contact")
    fun getAllContact(@Header("token") token: String):Call<List<ContactResponse>>

}