package com.example.contactofflineonline.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.contactofflineonline.data.local.entity.ContactEntity

@Dao
interface ContactDao {
    @Query("SELECT * FROM ContactEntity")
    fun getAllLocalContact():List<ContactEntity>
    @Delete
    fun deleteContact(contactEntity: ContactEntity)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContTact(contactEntity: ContactEntity)
}