package com.example.contactofflineonline.di

import com.example.contactofflineonline.domain.AppRepository
import com.example.contactofflineonline.domain.AppRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Singleton
    @Binds
    fun getAppRepository(impl:AppRepositoryImpl):AppRepository
}