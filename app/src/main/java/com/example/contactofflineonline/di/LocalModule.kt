package com.example.contactofflineonline.di

import android.content.Context
import androidx.room.Room
import com.example.contactofflineonline.data.local.dao.ContactDao
import com.example.contactofflineonline.data.local.database.MyDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
@Module
@InstallIn(SingletonComponent::class)
class LocalModule {
    @[Provides Singleton]
    fun provideMyDataBase(@ApplicationContext context: Context):MyDataBase=
        Room.databaseBuilder(context, MyDataBase::class.java,"Data2.db")
        .allowMainThreadQueries()
        .build()

    @[Provides Singleton]
    fun provideContactDao(myDataBase: MyDataBase):ContactDao=myDataBase.contactDao()
}