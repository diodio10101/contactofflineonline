package com.example.contactofflineonline.domain

import com.example.contact3.data.remote.request.CreateContactRequest
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contact3.data.remote.response.ContactUIData
import com.example.contectapi.data.remote.singup.LoginData
import com.example.contectapi.data.remote.singup.SMSCodeData
import com.example.contectapi.data.remote.singup.SingUpUserData

interface AppRepository {
    fun getAllContact(successBlock:(List<ContactUIData>)->Unit, errorBlock:(String)->Unit)
    fun addContact(data:CreateContactRequest,successBlock:()->Unit,errorBlock:(String)->Unit)
    fun syncWithServer(finishBlock: () -> Unit, errorBlock:(String) -> Unit)
    fun loginUser(data:LoginData,successBlock:()->Unit,errorBlock:(String)->Unit)
    fun singUpUser(data:SingUpUserData,successBlock:()->Unit,errorBlock:(String)->Unit)
    fun sentSms(data:SMSCodeData,successBlock:()->Unit,errorBlock:(String)->Unit)
    fun deleteContact(contactUIData: ContactUIData,successBlock: () -> Unit,errorBlock: (String) -> Unit)
    fun editContact(contactResponse: ContactResponse, successBlock: () -> Unit, errorBlock: (String) -> Unit)
}