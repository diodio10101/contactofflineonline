package com.example.contactofflineonline.domain

import com.example.contact3.data.remote.api.MyApi
import com.example.contact3.data.remote.request.CreateContactRequest
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contact3.data.remote.response.ContactUIData
import com.example.contact3.data.remote.response.toUIData
import com.example.contactofflineonline.data.local.dao.ContactDao
import com.example.contactofflineonline.data.local.entity.ContactEntity
import com.example.contactofflineonline.data.local.entity.toUIData
import com.example.contactofflineonline.data.model.enums.StatusEnum
import com.example.contactofflineonline.data.model.enums.toStatusEnum
import com.example.contactofflineonline.data.sourse.MyShar
import com.example.contactofflineonline.utils.NetworkStatusValidator
import com.example.contectapi.data.remote.singup.IdData
import com.example.contectapi.data.remote.singup.LoginData
import com.example.contectapi.data.remote.singup.SMSCodeData
import com.example.contectapi.data.remote.singup.SingUpUserData
import com.example.contectapi.data.remote.singup.StringData
import com.example.contectapi.data.remote.singup.TokenData
import com.example.nasiyaapp.utils.myLog
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryImpl @Inject constructor(
    private val myDao:ContactDao,
    private  val api: MyApi,
    private val gson:Gson,
    private val networkStatusValidator: NetworkStatusValidator
): AppRepository {
    override fun getAllContact(
        successBlock: (List<ContactUIData>) -> Unit, errorBlock: (String) -> Unit
    ) {
//        if (NetworkStatusValidator.hasInternet){
        api.getAllContact(MyShar.getToken()).enqueue(object : Callback<List<ContactResponse>> {
            override fun onResponse(
                call: Call<List<ContactResponse>>, response: Response<List<ContactResponse>>
            ) {
                if (response.isSuccessful || response.body() != null) {
                    successBlock.invoke(mergeData(response.body()!!, myDao.getAllLocalContact()))
                } else if (response.errorBody() != null) {
//                    errorBlock.invoke(gson.fromJson(response.errorBody()!!.string(), String::class.java))
                    errorBlock.invoke("Hatto!!")
                } else {
                    errorBlock.invoke("Error!!")
                }
            }

            override fun onFailure(call: Call<List<ContactResponse>>, t: Throwable) {
                errorBlock.invoke(t.message.toString())
            }

        })
//        }else{

//           successBlock.invoke(myDao.getAllLocalContact().map { it.toUIData(++index) })
//        }

    }

    override fun addContact(
        data: CreateContactRequest, successBlock: () -> Unit, errorBlock: (String) -> Unit
    ) {
        if (networkStatusValidator.hasInternet) {
            api.addContact(data, MyShar.getToken()).enqueue(object : Callback<ContactResponse> {
                override fun onResponse(
                    call: Call<ContactResponse>, response: Response<ContactResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        successBlock.invoke()
                    } else {
                        errorBlock.invoke("Error!")
                    }
                }

                override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                    errorBlock.invoke(t.message.toString())
                }

            })
        } else {
            myDao.insertContTact(
                ContactEntity(
                    0,
                    firstName = data.firstName,
                    lastName = data.lastName,
                    phone = data.phone,
                )
            )
            successBlock.invoke()
        }
    }

    override fun syncWithServer(finishBlock: () -> Unit, errorBlock: (String) -> Unit) {
        val list = myDao.getAllLocalContact()
//        list.forEach{
////        api.addContact(CreateContactRequest(it.firstName, it.lastName, it.phone),MyShar.getToken()).execute().body()
//        }
        list.forEach {
            myDao.deleteContact(it)
            if (it.status==3){
                api.editContact(ContactResponse(it.id,it.firstName,it.lastName,it.phone), MyShar.getToken())
                    .enqueue(object : Callback<ContactResponse> {
                        override fun onResponse(
                            call: Call<ContactResponse>, response: Response<ContactResponse>
                        ) {
                            if (response.isSuccessful && response.body() != null) {
                            } else if (response.errorBody() != null) {
                                errorBlock.invoke("Error!")
                            } else {
                                errorBlock.invoke("Hato!")
                            }
                        }

                        override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                            errorBlock.invoke(t.message.toString())
                        }

                    })
            }else{
                api.addContact(
                    CreateContactRequest(it.firstName, it.lastName, it.phone), MyShar.getToken()
                ).enqueue(object : Callback<ContactResponse> {
                    override fun onResponse(
                        call: Call<ContactResponse>,
                        response: Response<ContactResponse>,
                    ) {
                        if (response.isSuccessful && response.body() != null) {
                            finishBlock.invoke()
                        } else if (response.errorBody() != null) {
                            errorBlock.invoke("Hato")
                        } else errorBlock.invoke("Unknown error!!")
                    }

                    override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                        t.message?.let { message -> errorBlock.invoke(message) }
                    }
                })
            }

        }
        finishBlock.invoke()
    }

    override fun loginUser(
        data: LoginData, successBlock: () -> Unit, errorBlock: (String) -> Unit
    ) {
        api.loginUser(data).enqueue(object : Callback<TokenData> {
            override fun onResponse(call: Call<TokenData>, response: Response<TokenData>) {

                if (response.isSuccessful) {
                    TokenData(response.body()!!.token, response.body()!!.phone)
                    successBlock.invoke()
                } else {
                    errorBlock.invoke(response.errorBody()!!.string())
                }

            }

            override fun onFailure(call: Call<TokenData>, t: Throwable) {
                errorBlock.invoke(t.message.toString())
            }

        })
    }

    override fun singUpUser(
        data: SingUpUserData, successBlock: () -> Unit, errorBlock: (String) -> Unit
    ) {
        api.singUpUser(data).enqueue(object : Callback<StringData> {
            override fun onResponse(call: Call<StringData>, response: Response<StringData>) {
                if (response.isSuccessful) {
                    successBlock.invoke()
                } else {
                    errorBlock.invoke(response.errorBody()!!.string())
                }
            }

            override fun onFailure(call: Call<StringData>, t: Throwable) {
                errorBlock.invoke(t.message.toString())
            }

        })
    }

    override fun sentSms(
        data: SMSCodeData, successBlock: () -> Unit, errorBlock: (String) -> Unit
    ) {
        api.smsCode(data).enqueue(object : Callback<TokenData> {
            override fun onResponse(call: Call<TokenData>, response: Response<TokenData>) {
                if (response.isSuccessful) {
                    successBlock.invoke()
                    TokenData(response.body()!!.token, response.body()!!.phone)
                } else {
                    errorBlock.invoke(response.errorBody()!!.string())
                }
            }

            override fun onFailure(call: Call<TokenData>, t: Throwable) {
                errorBlock.invoke(t.message.toString())
            }

        })
    }

    var index = 0
    private fun mergeData(
        remoteList: List<ContactResponse>, localList: List<ContactEntity>
    ): List<ContactUIData> {
        val result = ArrayList<ContactUIData>()
        result.addAll(remoteList.map { it.toUIData() })

        index = remoteList.lastOrNull()?.id ?: 0      // face
        localList.forEach { entity ->
            when (entity.status.toStatusEnum()) {
                StatusEnum.ADD -> {
                    result.add(entity.toUIData(++index))
                }

                StatusEnum.EDIT -> {
                    val data = result.find { item -> item.id == entity.id }
                    val pos = result.indexOf(data)
                    data.toString().myLog()
                    result[pos] = entity.toUIData(entity.id)
                }

                StatusEnum.DELETE -> {
                    val findData = result.find { it.id == entity.remoteID }
                    if (findData != null) {
//                        val findIndex = result.indexOf(findData)
                        api.deleteContact(findData.id, MyShar.getToken())
                        result.remove(findData)
//                        val newData = entity.toUIData(findData.id)
//                        result[findIndex]= newData
                    }
                }

                StatusEnum.DEF -> {}
            }
        }

        return result
    }

    override fun deleteContact(
        contactUIData: ContactUIData, successBlock: () -> Unit, errorBlock: (String) -> Unit
    ) {
        if (networkStatusValidator.hasInternet) {
            api.deleteContact(contactUIData.id, MyShar.getToken())
                .enqueue(object : Callback<IdData> {
                    override fun onResponse(call: Call<IdData>, response: Response<IdData>) {
                        if (response.isSuccessful && response.body() != null) {
                            successBlock.invoke()
                        } else if (response.errorBody() != null) {
                            errorBlock.invoke("Error!")
                        } else {
                            errorBlock.invoke("Hato!")
                        }
                    }

                    override fun onFailure(call: Call<IdData>, t: Throwable) {
                        errorBlock.invoke(t.message.toString())
                    }

                })
        } else {
            contactUIData.apply {
                myDao.insertContTact(ContactEntity(id, firstName, lastName, phone, id, 2))
                successBlock.invoke()
            }
        }
    }

    override fun editContact(
        contactResponse: ContactResponse, successBlock: () -> Unit, errorBlock: (String) -> Unit
    ) {
        if (networkStatusValidator.hasInternet) {
            api.editContact(contactResponse, MyShar.getToken())
                .enqueue(object : Callback<ContactResponse> {
                    override fun onResponse(
                        call: Call<ContactResponse>, response: Response<ContactResponse>
                    ) {
                        if (response.isSuccessful && response.body() != null) {
                            successBlock.invoke()
                        } else if (response.errorBody() != null) {
                            errorBlock.invoke("Error!")
                        } else {
                            errorBlock.invoke("Hato!")
                        }
                    }

                    override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                        errorBlock.invoke(t.message.toString())
                    }

                })
        } else {
            contactResponse.apply {
                myDao.insertContTact(ContactEntity(id, firstName, lastName, phone, id, 3))
                successBlock.invoke()
            }
        }
    }

}