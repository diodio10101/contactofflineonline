package com.example.contactofflineonline.app

import android.app.Application
import com.example.contactofflineonline.data.sourse.MyShar
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App:Application() {
    override fun onCreate() {
        super.onCreate()
        MyShar.init(this)
    }
}