package com.example.contactofflineonline.presenter.screen.main

import androidx.lifecycle.LiveData
import com.example.contact3.data.remote.request.CreateContactRequest
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contact3.data.remote.response.ContactUIData

interface MainVm {
    val progressLiveData: LiveData<Boolean>
    val contactsLiveData: LiveData<List<ContactUIData>>
    val errorMessageLiveData: LiveData<String>
    val notConnectionLiveData: LiveData<Unit>
    val emptyStateLiveData: LiveData<Boolean>
    fun addContact(createContactRequest: CreateContactRequest)

    val openAddDialogLiveData:LiveData<Unit>

    fun loadContacts()

    fun openAddDialog()
    fun editContact(contactResponse: ContactResponse)
    fun deleteContact(contactUIData: ContactUIData)
}