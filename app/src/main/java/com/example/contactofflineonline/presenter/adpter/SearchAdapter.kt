package com.example.contact3.presenter.adpter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.contact2.data.model.inflate
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contactofflineonline.R

class SearchAdapter(var data:ArrayList<ContactResponse>): Adapter<SearchAdapter.Holder>() {
    lateinit var onSelectInfo:(index:Int)->Unit
    inner class Holder(view: View):ViewHolder(view) {
        var image=view.findViewById<ImageView>(R.id.image)
        var name=view.findViewById<TextView>(R.id.name)
        var phone=view.findViewById<TextView>(R.id.phone)
        var line=view.findViewById<View>(R.id.line_bottom)
        var info=view.findViewById<ConstraintLayout>(R.id.info_search)
        fun bind(item:ContactResponse){
            name.text=item.firstName+""+item.lastName
            phone.text=item.phone
            line.visibility=View.VISIBLE
            info.setOnClickListener {
                onSelectInfo.invoke(adapterPosition)
            }
        }
        fun isInVisibility(){
            line.visibility=View.INVISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder = Holder(parent.inflate(R.layout.item_search))
    override fun getItemCount(): Int =data.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position])
        if (position==data.size-1){
            holder.isInVisibility()
        }
    }
}