package com.example.contectapi.presenter.screen.singup

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.contactofflineonline.domain.AppRepository
import com.example.contectapi.data.remote.singup.SingUpUserData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SingUpVM @Inject constructor(
    private val appRepository:AppRepository
) :ViewModel() {
    val singUp=MutableLiveData<Boolean>()
    val login=MutableLiveData<Unit>()
    fun onClickSingUp(data:SingUpUserData){
       appRepository.singUpUser(data=data,
           successBlock = {
                  singUp.value=true
       },
           errorBlock = {
           singUp.value=false
       })
    }
    fun onClickLogin(){
        login.value=Unit
    }
    }