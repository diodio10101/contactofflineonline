package com.example.contectapi.presenter.screen.sms

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.contactofflineonline.domain.AppRepository
import com.example.contectapi.data.remote.singup.SMSCodeData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SmsVM @Inject constructor(
    private val appRepository:AppRepository
):ViewModel() {
    val nextSms=MutableLiveData<Boolean>()
    val sendMessage=MutableLiveData<Unit>()

    fun onClickNext(data:SMSCodeData){
        appRepository.sentSms(data=data,
            successBlock = {
                nextSms.value=true
            },
            errorBlock = {
                nextSms.value=false
            })
    }
    fun onClickMessage(){
        sendMessage.value=Unit
    }
}