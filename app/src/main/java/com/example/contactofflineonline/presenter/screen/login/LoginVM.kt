package com.example.contectapi.presenter.screen.login

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.contactofflineonline.domain.AppRepository
import com.example.contectapi.data.remote.singup.LoginData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginVM @Inject constructor(
    val appRepository:AppRepository
):ViewModel()  {
    val login=MutableLiveData<Boolean>()
    val singUp=MutableLiveData<Unit>()
    fun onClickLogin(data: LoginData,view:View){
        appRepository.loginUser(
            data = data,
            successBlock = {
                login.value=true
                view.isClickable=true
            },
            errorBlock = {
                login.value=false
                view.isClickable=true
            }
        )
//        api.loginUser(data).enqueue(object :Callback<TokenData>{
//            override fun onResponse(call: Call<TokenData>, response: Response<TokenData>) {
//                view.isClickable=true
//                if (response.isSuccessful){
//                    TokenData(response.body()!!.token,response.body()!!.phone)
//                    login.value=true
//                }else{
//                    login.value=false
//                }
//
//            }
//
//            override fun onFailure(call: Call<TokenData>, t: Throwable) {
//                login.value=false
//                view.isClickable=true
//            }
//
//        })
    }
    fun onClickSingUp(){
        singUp.value=Unit
    }
}