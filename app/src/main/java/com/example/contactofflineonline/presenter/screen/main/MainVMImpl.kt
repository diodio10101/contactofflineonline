package com.example.contactofflineonline.presenter.screen.main

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Insert
import com.example.contact3.data.remote.request.CreateContactRequest
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contact3.data.remote.response.ContactUIData
import com.example.contactofflineonline.domain.AppRepository
import com.example.contactofflineonline.domain.AppRepositoryImpl
import com.example.nasiyaapp.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
@HiltViewModel
class MainVMImpl @Inject constructor(
    private var appRepository:AppRepository
) :ViewModel(),MainVm {
    override val progressLiveData= MutableLiveData<Boolean>()
    override val contactsLiveData=MutableLiveData<List<ContactUIData>>()
    override val errorMessageLiveData=MutableLiveData<String>()
    override val notConnectionLiveData=MutableLiveData<Unit>()
    override val emptyStateLiveData =MutableLiveData<Boolean>()
    override val openAddDialogLiveData=MutableLiveData<Unit>()

    override fun addContact(createContactRequest: CreateContactRequest) {
        appRepository.addContact(createContactRequest,
            successBlock = {
                "koshdi".myLog()
                loadContacts()
            },
            errorBlock = {

                errorMessageLiveData.value=it
            })
    }


    @SuppressLint("SuspiciousIndentation")
    override fun loadContacts() {

        progressLiveData.value=true
        appRepository.getAllContact(
            successBlock = {
                progressLiveData.value=false
                   if (it.isEmpty())emptyStateLiveData.value=true
                else {
                    emptyStateLiveData.value=false
                }
                contactsLiveData.value=it
            },
            errorBlock = {
                progressLiveData.value=false
                errorMessageLiveData.value=it
            })
    }

    override fun openAddDialog() {
        openAddDialogLiveData.value=Unit
    }
    override fun deleteContact(contactUIData: ContactUIData){
        appRepository.deleteContact(contactUIData=contactUIData,
            successBlock = {
                loadContacts()
            }, errorBlock = {
                errorMessageLiveData.value=it
            }
        )
    }
    override fun editContact(contactResponse: ContactResponse){
        appRepository.editContact(
            contactResponse = contactResponse,
            successBlock = {
                loadContacts()
            },
            errorBlock = {
                errorMessageLiveData.value=it
            }
        )
    }


}