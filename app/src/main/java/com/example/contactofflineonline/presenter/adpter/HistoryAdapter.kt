package com.example.contact3.presenter.adpter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.contact2.data.model.inflate
import com.example.contactofflineonline.R

class HistoryAdapter(var data:ArrayList<String>):Adapter<HistoryAdapter.Holder>() {
    lateinit var onSelectInfoSearch:(String)->Unit
    inner class Holder(view:View):ViewHolder(view) {
        var txt=view.findViewById<TextView>(R.id.txtText)
        var delete=view.findViewById<TextView>(R.id.delete)
        var background=view.findViewById<ConstraintLayout>(R.id.info_search)
        var line=view.findViewById<View>(R.id.line_bottom)
        init {
            delete.setOnClickListener {
                data.removeAt(adapterPosition)
               notifyDataSetChanged()
            }
            background.setOnClickListener {
                onSelectInfoSearch.invoke(txt.text.toString())
            }
        }
        fun bind(item:String){
            txt.text=item
            if (data.size==1){
                background.setBackgroundResource(R.drawable.backgrount_hictory)
                line.visibility=View.INVISIBLE
            }else if(adapterPosition==0){
                background.setBackgroundResource(R.drawable.top_backgrount_hictory)
                line.visibility=View.VISIBLE
            }else if (adapterPosition==data.size-1){
                background.setBackgroundResource(R.drawable.bottom_backgrount_hictory)
                line.visibility=View.INVISIBLE
            }else{
                line.visibility=View.VISIBLE
                background.setBackgroundResource(R.color.white)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder = Holder(parent.inflate(R.layout.item_history))

    override fun getItemCount(): Int=data.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position])



    }
}