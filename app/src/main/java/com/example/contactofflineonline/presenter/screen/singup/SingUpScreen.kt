package com.example.contectapi.presenter.screen.singup

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.contactofflineonline.R
import com.example.contactofflineonline.databinding.ScreenSignupBinding
import com.example.contectapi.data.remote.singup.SingUpUserData
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SingUpScreen:Fragment() {
    private var _binding: ScreenSignupBinding?=null
    private val binding by lazy { _binding!! }
    private val viewModel by viewModels<SingUpVM>()
    private val onClickSingUp=Observer<Boolean>{
        if (it){
            findNavController().navigate(R.id.action_singUpScreen_to_SMSScreen)
        }else{
            Toast.makeText(requireContext(),"ERROR", Toast.LENGTH_SHORT).show()
        }
    }
    private val onClickLogin=Observer<Unit>{
        findNavController().navigate(R.id.action_singUpScreen_to_loginScreen)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding= ScreenSignupBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initButton()
    }
    @SuppressLint("FragmentLiveDataObserve")
    fun initViewModel(){
        viewModel.singUp.observe(this,onClickSingUp)
        viewModel.login.observe(this,onClickLogin)
    }
    fun initButton(){
        binding.apply {
            singupButton.setOnClickListener {
                viewModel.onClickSingUp(SingUpUserData(firstname.text.toString(),lastname.text.toString(),phone.text.toString(),password.text.toString()))
            }
            login.setOnClickListener {
                viewModel.onClickLogin()
            }
        }
    }
}