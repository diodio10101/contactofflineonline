package com.example.contactofflineonline.presenter.screen.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contact3.data.remote.response.ContactUIData
import com.example.contact3.presenter.adpter.MainScreenAdapter
import com.example.contact3.presenter.screen.dialog.ContactAddDialog
import com.example.contactofflineonline.Birbolo
import com.example.contactofflineonline.R
import com.example.contactofflineonline.data.sourse.MyShar
import com.example.contactofflineonline.databinding.ScreenMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainScreen:Fragment() {
    private var _binding:ScreenMainBinding?=null
    private val binding by lazy { _binding!! }
    private val viewModel:MainVm by viewModels<MainVMImpl>()
    private val adapter by lazy { MainScreenAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding=ScreenMainBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        initButtons()
        initViewModel()
        Birbolo.remove={
            requireActivity().runOnUiThread {
                viewModel.loadContacts()
            }
        }
        requireActivity().runOnUiThread {
            viewModel.loadContacts()
        }
    }
    fun initButtons(){
        binding.apply {
            recyclerView.adapter=adapter
            swipeRefreshLayout.setOnRefreshListener {
                requireActivity().runOnUiThread {
                    viewModel.loadContacts()
                    empty.isVisible=false
                }

            }
            add.setOnClickListener {
                viewModel.openAddDialog()
            }
            logOut.setOnClickListener {
                MyShar.setToken("")
                findNavController().navigate(R.id.action_mainScreen_to_loginScreen)
            }
            adapter.onSelectDelete={
                viewModel.deleteContact(it)
            }
            adapter.onSelectEdit={
                openDialog(ContactResponse(it.id,it.firstName,it.lastName,it.phone))
            }
            adapter.onSelectInfo={

            }
        }
    }
    @SuppressLint("FragmentLiveDataObserve")
    fun initViewModel(){
        viewModel.contactsLiveData.observe(viewLifecycleOwner,contactsObserver)
        viewModel.emptyStateLiveData.observe(viewLifecycleOwner,emptyStateObserver)
        viewModel.progressLiveData.observe(viewLifecycleOwner,progressObserver)
        viewModel.openAddDialogLiveData.observe(this,openAddDialogObserver)
        viewModel.errorMessageLiveData.observe(viewLifecycleOwner,errorMessageObserver)
//        viewModel.loadContacts()
    }
    private fun openDialog(contactResponse: ContactResponse){
        val dialog=ContactAddDialog(1)
        dialog.setContact(contactResponse)
        dialog.show(requireActivity().supportFragmentManager,"DialogAdd")
        dialog.onClickSave1={
            viewModel.editContact(it)
        }
    }
    private val contactsObserver=Observer<List<ContactUIData>>{
        adapter.submitList(it)
    }
    private val emptyStateObserver=Observer<Boolean>{
        binding.empty.isVisible=it
    }
    private val progressObserver=Observer<Boolean>{
        binding.swipeRefreshLayout.isRefreshing=it
    }
    private val openAddDialogObserver=Observer<Unit>(){
        val dialog=ContactAddDialog()
        dialog.show(requireActivity().supportFragmentManager,"")
        dialog.onClickSave={
            viewModel.addContact(it)
        }
    }
    private val errorMessageObserver=Observer<String>{
        Toast.makeText(requireContext(),it,Toast.LENGTH_SHORT).show()
    }
}