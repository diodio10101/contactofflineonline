package com.example.contact3.presenter.adpter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.contact3.data.model.SMSData
import com.example.contactofflineonline.databinding.ItemSmsBinding
import java.text.SimpleDateFormat

class ChatAdapter(var data:ArrayList<SMSData>):Adapter<ChatAdapter.Holder>() {
    class Holder(var binding: ItemSmsBinding):ViewHolder(binding.root) {
        @SuppressLint("SimpleDateFormat")
        fun bind(item:SMSData){
            val s=SimpleDateFormat("hh:mm").format(item.date)
            binding.message.text=item.message
            binding.time.text=s
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder=
        Holder(ItemSmsBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun getItemCount(): Int=data.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position])
    }

}