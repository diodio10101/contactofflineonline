package com.example.contact3.presenter.adpter

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.contact2.data.model.inflate
import com.example.contactofflineonline.R

class CallHistoryAdapter(var data:ArrayList<String>):Adapter<CallHistoryAdapter.Holder>() {
    inner class Holder(view:View):ViewHolder(view){
        var time:TextView=view.findViewById(R.id.day)
        var background:ConstraintLayout=view.findViewById(R.id.history_background)
        var line:View=view.findViewById(R.id.line_bottom)
        fun bind(item:String){
            time.text=item
            when(adapterPosition){
                0->{
                    if(data.size!=1){
                        line.visibility=View.VISIBLE
                        background.setBackgroundResource(R.drawable.top_backgrount_hictory)
                    }else{
                        line.visibility=View.INVISIBLE
                        background.setBackgroundResource(R.drawable.backgrount_hictory)
                    }

                }
                data.size-1->{
                    line.visibility=View.INVISIBLE
                    background.setBackgroundResource(R.drawable.bottom_backgrount_hictory)
                }
                else->{
                    line.visibility=View.VISIBLE
                    background.setBackgroundResource(R.color.white)
                }
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder{

     return Holder(parent.inflate(R.layout.item_call_history))
    }

    override fun getItemCount(): Int =data.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
            holder.bind(data[position].substring(3))
    }
}

