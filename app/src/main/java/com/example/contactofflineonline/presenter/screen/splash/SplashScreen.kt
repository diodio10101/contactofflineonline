package com.example.contactofflineonline.presenter.screen.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.contactofflineonline.R
import com.example.contactofflineonline.data.sourse.MyShar
import com.example.contactofflineonline.databinding.ScreenSplashBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
@AndroidEntryPoint
class SplashScreen:Fragment() {
    private var _binding:ScreenSplashBinding?=null
    private val binding by lazy { _binding!! }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding=ScreenSplashBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            delay(1000)
            openScreen()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
    private fun openScreen(){
        if (MyShar.getToken()==""){
            findNavController().navigate(R.id.action_splashScreen_to_loginScreen)
        }else{
            findNavController().navigate(R.id.action_splashScreen_to_mainScreen)
        }
    }
}