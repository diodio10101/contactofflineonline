package com.example.contact3.presenter.adpter

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.chauthai.swipereveallayout.SwipeRevealLayout
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.example.contact2.data.model.inflate
import com.example.contact3.data.remote.response.ContactUIData
import com.example.contactofflineonline.R
import com.example.contactofflineonline.data.model.enums.StatusEnum

class MainScreenAdapter(): ListAdapter<ContactUIData,MainScreenAdapter.Holder>(ContactDiffUtil) {
    private val viewBinderHelper = ViewBinderHelper()
     var onSelectDelete:((contactUIData: ContactUIData)->Unit)?=null
     var onSelectEdit:((ContactUIData)->Unit)?=null
     var onSelectInfo:((ContactUIData)->Unit)?=null
    inner class Holder(view: View):ViewHolder(view) {
        var swipe: SwipeRevealLayout =view.findViewById(R.id.swipe)
        var image: ImageView =view.findViewById(R.id.image)
        var txtName: TextView =view.findViewById(R.id.name)
        var txtPhone: TextView =view.findViewById(R.id.phone)
        var delete: FrameLayout =view.findViewById(R.id.delete)
        var edit: FrameLayout =view.findViewById(R.id.edit)
        var textStatus:TextView=view.findViewById(R.id.textStatus)
        init {
            delete.setOnClickListener {
                onSelectDelete?.invoke(getItem(adapterPosition))
                swipe.close(true)
            }
            edit.setOnClickListener {
                onSelectEdit?.invoke(getItem(adapterPosition))
                swipe.close(true)
            }
            view.findViewById<ConstraintLayout>(R.id.bosildi).setOnClickListener {
                onSelectInfo?.invoke(getItem(adapterPosition))
            }
        }
        fun bind(item:ContactUIData){
            image.setImageResource(R.drawable.image2)
            txtName.text=item.firstName+" "+item.lastName
            txtPhone.text=item.phone
            when(item.status){
                StatusEnum.DEF->textStatus.visibility=View.INVISIBLE
                else ->{
                    textStatus.visibility=View.VISIBLE
                }
            }
            textStatus.text=item.status.name
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder = Holder(parent.inflate(
        R.layout.item_contact
    ))


    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
        viewBinderHelper.bind(holder.swipe, getItem(position).firstName)
    }
    object ContactDiffUtil:DiffUtil.ItemCallback<ContactUIData>() {
        override fun areItemsTheSame(oldItem: ContactUIData, newItem: ContactUIData): Boolean =oldItem.id==newItem.id

        override fun areContentsTheSame(oldItem: ContactUIData, newItem: ContactUIData): Boolean =oldItem==newItem

    }

}