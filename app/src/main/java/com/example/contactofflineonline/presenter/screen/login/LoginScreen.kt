package com.example.contectapi.presenter.screen.login

import android.annotation.SuppressLint
import android.icu.text.LocaleDisplayNames
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.contactofflineonline.R
import com.example.contactofflineonline.databinding.ScreenLoginBinding
import com.example.contectapi.data.remote.singup.LoginData
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginScreen:Fragment() {
    private var _binding: ScreenLoginBinding?=null
    private val binding by lazy { _binding!! }
    private val viewModel by viewModels<LoginVM>()
    private val onClickLogin= Observer<Boolean> {
        if (it){
            findNavController().navigate(R.id.action_loginScreen_to_mainScreen)
        }else{
            Toast.makeText(requireContext(),"ERROR",Toast.LENGTH_SHORT).show()
        }
    }
    private val onClickSingUp=Observer<Unit>{
        findNavController().navigate(R.id.action_loginScreen_to_singUpScreen)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding= ScreenLoginBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initButton()
    }
    @SuppressLint("FragmentLiveDataObserve")
    fun initViewModel(){
        viewModel.login.observe(this,onClickLogin)
        viewModel.singUp.observe(this,onClickSingUp)

    }
    fun initButton(){
        binding.apply {
            loginButton.setOnClickListener {
                loginButton.isClickable=false
                viewModel.onClickLogin(LoginData(phone.text.toString(),password.text.toString()),loginButton)
            }
            singup.setOnClickListener {
                viewModel.onClickSingUp()
            }
        }
    }
}