package com.example.contactofflineonline.presenter.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.contactofflineonline.Birbolo
import com.example.contactofflineonline.R
import com.example.contactofflineonline.domain.AppRepository
import com.example.contactofflineonline.domain.AppRepositoryImpl
import com.example.contactofflineonline.utils.NetworkStatusValidator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject  lateinit var appRepository:AppRepository
    @Inject  lateinit var networkStatusValidator: NetworkStatusValidator
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        networkStatusValidator.init(
            block = {
                appRepository.syncWithServer(finishBlock = {
                    Birbolo.remove?.invoke()
                },
                    errorBlock = {
                        Toast.makeText(this,"OOOOOO cho bla",Toast.LENGTH_SHORT).show()
                    }
                )
            }
        )
    }
}