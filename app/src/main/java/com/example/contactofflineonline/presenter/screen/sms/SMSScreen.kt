package com.example.contectapi.presenter.screen.sms

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.contactofflineonline.R
import com.example.contactofflineonline.databinding.ScreenSmsBinding
import com.example.contectapi.data.remote.singup.SMSCodeData
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SMSScreen:Fragment() {
    private var _binding: ScreenSmsBinding?=null
    private val binding by lazy { _binding!! }
    private val viewModel by viewModels<SmsVM>()
    private val onClickSendMessage=Observer<Unit>{

    }
    private val onClickNext=Observer<Boolean>{
        if (it){
            findNavController().navigate(R.id.action_SMSScreen_to_mainScreen)
        }else{
            Toast.makeText(requireContext(),"ERROR!",Toast.LENGTH_SHORT).show()
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding= ScreenSmsBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initButton()
    }
    private fun initButton(){
        binding.apply {
            loginButton.setOnClickListener {
                viewModel.onClickNext(SMSCodeData(phone.text.toString(),password.text.toString()))
            }

        }
    }
    @SuppressLint("FragmentLiveDataObserve")
    private fun initViewModel(){
        viewModel.sendMessage.observe(this,onClickSendMessage)
        viewModel.nextSms.observe(this,onClickNext)
    }
}