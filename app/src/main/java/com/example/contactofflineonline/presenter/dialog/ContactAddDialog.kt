package com.example.contact3.presenter.screen.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.contact3.data.remote.request.CreateContactRequest
import com.example.contact3.data.remote.response.ContactResponse
import com.example.contactofflineonline.databinding.DialogAddContactBinding

class ContactAddDialog(val index:Int=0):DialogFragment() {
    private var _binding: DialogAddContactBinding?=null
    private val binding by lazy { _binding!! }
    private var id=0
    private var firstName1=""
    private var lastName1=""
    private var phone1=""
    var onClickSave:((CreateContactRequest)->Unit)?=null
    var onClickSave1:((ContactResponse)->Unit)?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding=DialogAddContactBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        innitButtons()
        loadText()
    }
    fun innitButtons(){
        binding.apply {
            save.setOnClickListener {
                if (!isFirstName())return@setOnClickListener
                if (!isLastName())return@setOnClickListener
                if (!isPhone())return@setOnClickListener
                if (index==0){
                    val contact=CreateContactRequest(firstname.text.toString(),lastname.text.toString(),phone.text.toString())
                    onClickSave?.invoke(contact)
                }else{
                    val contact=ContactResponse(id,firstname.text.toString(),lastname.text.toString(),phone.text.toString())
                    onClickSave1?.invoke(contact)
                }
                dismiss()
            }

        }
    }
    fun isFirstName():Boolean{
        var firsName=binding.firstname.text
        if (firsName.isEmpty())return false
        return firsName.length >= 4
    }
    fun isLastName():Boolean{
        var lastname=binding.lastname.text
        if (lastname.isEmpty())return false
        return lastname.length >= 4
    }
    fun isPhone():Boolean{
        var phone=binding.phone.text
        if (phone.isEmpty())return false
        return phone.length == 13
    }
    fun setContact(contact:ContactResponse){
            id=contact.id
            firstName1=contact.firstName
            lastName1=contact.lastName
            phone1=contact.phone

    }
    fun loadText(){
        binding.apply {
            firstname.setText(firstName1)
            lastname.setText(lastName1)
            phone.setText(phone1)
        }
    }

}